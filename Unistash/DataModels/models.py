from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


# Create your models here.
     
# first table of course

class MainCourse(models.Model):
    coursename = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse('DataModels:detail' ,kwargs={'pk': self.pk})

    def __str__(self):
        return self.coursename 

class Course(models.Model):
    course = models.CharField(max_length=100)
    branch = models.CharField(max_length=100)
    num_sem =  models.IntegerField()

    def get_absolute_url(self):
        return reverse('DataModels:detail' ,kwargs={'pk': self.pk})


    def __str__(self):
        return self.course + '('+self.branch +')'

# for identifying which year material user wanna see

class year_notes(models.Model):
    courseid = models.ForeignKey(Course, on_delete=models.CASCADE)
    yearofnotes = models.IntegerField()
    sem = models.IntegerField()

    def get_absolute_url(self):
        return reverse('DataModels:year' ,kwargs={'pk': self.courseid})


    def __str__(self):
        return self.courseid.branch +' ('+str(self.sem)+')'


# for a particular year particular subject

class subject(models.Model):
     semid = models.ForeignKey(year_notes)
     sub_id = models.CharField(max_length=50)
     sub = models.CharField(max_length=100)
     Exam_type = models.CharField(max_length=100)

     def __str__(self):
        return self.semid.courseid.branch +' ('+self.sub+' ' + self.Exam_type+')'

# For a particular subject with a particular term(minor/Major) [Exam_type]
# We have a different column one for notes , one for exam papers and another for
# practical files

class Data(models.Model):
     primary_k = models.ForeignKey(subject)
     notes = models.FileField()
     exam_papers = models.FileField()
     practical_files = models.FileField(blank=True, null=True)

     def __str__(self):
        return self.primary_k.sub+'( '+self.primary_k.Exam_type+')'
