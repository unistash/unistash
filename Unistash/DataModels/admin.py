from django.contrib import admin
from .models import Course ,year_notes,subject,Data,MainCourse

# Register your models here.
admin.site.register(Course)
admin.site.register(year_notes)
admin.site.register(subject)
admin.site.register(Data)
admin.site.register(MainCourse)
