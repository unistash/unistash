from django.views import generic
#require a form to create a new object
from django.views.generic.edit import CreateView,UpdateView ,DeleteView
# for uploading form
from django.core.urlresolvers import reverse_lazy
# for login
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login
from django.http import HttpResponse



# Create your views here.
from .models import MainCourse,Course,year_notes
from django.views.generic import View


class IndexView(generic.ListView):
        template_name = 'DataModels/index.html'
        context_object_name = 'branches'

        def get_queryset(self):
                return MainCourse.objects.all()

class DetailView(generic.DetailView):
        template_name = 'DataModels/detail.html'
        context_object_name = 'Course'

        def get_queryset(self):
                return Course.objects.all()

class Detail1View(generic.ListView):
        template_name = 'DataModels/year.html'
        context_object_name = 'year'

        def get_queryset(self):
                y=year_notes.objects.filter(courseid=1)
                return y
      
