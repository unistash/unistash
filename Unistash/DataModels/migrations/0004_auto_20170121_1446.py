# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-21 09:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DataModels', '0003_auto_20170119_1244'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainCourse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('coursename', models.CharField(max_length=100)),
            ],
        ),
        migrations.AlterField(
            model_name='data',
            name='practical_files',
            field=models.FileField(blank=True, null=True, upload_to=b''),
        ),
    ]
