from django.conf.urls import url
from . import views


app_name='DataModels'

urlpatterns = [

    #main data page
    url(r'^$', views.IndexView.as_view() , name='index'),

    #detailed page with branches of the courses
    url(r'^course/(?P<pk>[0-9]+)/$', views.DetailView.as_view() , name='detail'),

     url(r'^2017/cse/$', views.Detail1View.as_view() , name='year'),


]
